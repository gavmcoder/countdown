<?php

namespace CountDownBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        $datenow = new \DateTime();

        $datefuture = new \Datetime('2019-12-25');

        $days = $datenow->diff($datefuture);

        return $this->render('CountDownBundle:Default:index.html.twig',array('countdown' => $days));
    }
}
